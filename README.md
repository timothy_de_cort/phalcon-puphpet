# README #

* Minimum required Vagrant version is 1.7.4 ( https://www.vagrantup.com/downloads.html )
* VirtualBox 5 is now the minimum required version! ( https://www.virtualbox.org/wiki/Downloads )


Just pull the repo and cd to the directory via cmd.

Execute "vagrant up" and wait for the installation to complete. This could take up to 10ish minutes.

If you would like to change the "demo" settings and create your own project, I recommend you open /puphpet/config.yaml and just do a *search and replace* "demo" for your project name.

Doing so, will replace the default database, database user and password, project directories, ... etc

Make sure to edit the config.ini files inside the /app/config directory to reflect your change.