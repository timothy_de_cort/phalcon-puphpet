-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

use demo;

INSERT INTO `oauth` (`id`, `name`) VALUES
(1,	'default'),
(2,	'facebook'),
(3,	'twitter');

INSERT INTO `role` (`id`, `name`, `label`) VALUES
(1,	'admin',	'beheerder'),
(2,	'user',	'guest'),
(3,	'public',	'public');

INSERT INTO `user` (`id`, `name`, `email`, `password`, `oauth_id`, `oauth_uid`, `admin`) VALUES
(1,	'test',	'test@test.com',	'$2a$11$4v0Ln55pyOPJwDCd1iyqDOLiPWyT1KLETgCe8u0GjPRQEJ6Gg9o6W',	1,	0,	1);
