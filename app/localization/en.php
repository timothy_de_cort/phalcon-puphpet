<?php

$translations = [
    'CONNECT_ME'                          => 'Connect me',
    'EMAIL_PASSWORD_COMBINATION_MISMATCH' => 'Email and password combination didn\'t match.',
    'HOME'                                => 'Home',
    'LOGOUT'                              => 'Logout',
    'NAME'                                => 'Name',
    'NO_MORE_RECORDS_FOUND'               => 'No more %records% found.',
    'SEE_YOU_NEXT_TIME_USER'              => 'See you next time, %name%',
    'SOMETHING_WENT_WRONG'                => 'Something went wrong.',
    'SOMETHING_WENT_WRONG_TRY_AGAIN'      => 'Something went wrong on our side. Please try again later.',
    'VALUE_IS_REQUIRED'                   => '%value% is required.',
    'WELCOME_USER'                        => 'Welcome %name%',
];