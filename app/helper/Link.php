<?php

use Phalcon\Tag;

/**
 * Class Link
 *
 * @author Timothy De Cort <mail@timothydc.be>
 */
class Link extends Tag
{
    /** @var string */
    protected $_id = '';
    /** @var string */
    protected $_icon = '';
    /** @var string */
    protected $_class = '';
    /** @var string */
    protected $_label = '';
    /** @var string */
    protected $_controller = '';
    /** @var string */
    protected $_action = '';
    /** @var string */
    protected $_prefix = '';
    /** @var string */
    protected $_url = '';
    /** @var bool */
    protected $_modal = false;
    /** @var array */
    protected $_properties = [];
    /** @var array */
    protected $_parameters = [];

    public function initDefaults()
    {
        $this->_id         = '';
        $this->_icon       = '';
        $this->_class      = '';
        $this->_label      = '';
        $this->_controller = '';
        $this->_action     = '';
        $this->_prefix     = '';
        $this->_url        = '';
        $this->_modal      = false;
        $this->_properties = [];
        $this->_parameters = [];
    }

    /**
     * @return string
     */
    public function render()
    {
        $link = '';

        if ($this->isModal() || SecurityPlugin::isAllowed($this->getController(), $this->getAction())) {
            $link = '<a href="' . $this->getLink() . '"' . $this->getProperties() . '>' . $this->getLabel() . '</a>';
        }

        return $link;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->_icon ? '<i class="' . $this->_icon . '"></i>&nbsp;' : '';
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->_icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->_class ? ' class="' . $this->_class . '"' : '';
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->_class = $class;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->getIcon() . $this->_label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->_label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getController()
    {
        if ($this->_controller == 'index') {
            return '';
        }

        return $this->_controller;
    }

    /**
     * @param string $controller
     * @return $this
     */
    public function setController($controller)
    {
        $this->_controller = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        if ($this->_action == 'index') {
            return '';
        }

        return $this->_action;
    }

    /**
     *
     * @param string $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->_action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->_prefix;
    }

    /**
     * @param string $prefix
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->_prefix = $prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        if ($this->_modal) {
            return 'javascript:;';
        }

        $prefix     = $this->getPrefix() ? '/' . $this->getPrefix() : '';
        $controller = $this->getController() && $this->getController() != 'index' ? '/' . $this->getController() : '';
        $action     = $this->getAction() && $this->getAction() != 'index' ? '/' . $this->getAction() : '';

        $url = $prefix . $controller . $action;
        if (empty($prefix) && empty($controller) && empty($action)) {
            $url = '/';
        }

        if (!empty($this->getUrl())) {
            $url = $prefix . '/' . $this->getUrl();
        }

        return $url . $this->getParameters();
    }

    /**
     * @return boolean
     */
    public function isModal()
    {
        return $this->_modal;
    }

    /**
     * @param bool $modal
     * @return $this
     */
    public function setModal($modal)
    {
        $this->_modal = $modal;
        return $this;
    }

    /**
     * @return string
     */
    public function getProperties()
    {
        $properties = $this->_properties;

        if (is_array($this->_properties)) {
            $properties = '';
            foreach ($this->_properties as $key => $value) {
                $properties .= ' ' . $key . '="' . $value . '"';
            }
        }

        return $this->getClass() . $properties;
    }

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties($properties)
    {
        $this->_properties = $properties;
        return $this;
    }

    /**
     * @return string
     */
    public function getParameters()
    {
        $parameters = $this->_parameters;

        if (is_array($this->_parameters)) {
            $parameters = '';
            foreach ($this->_parameters as $key => $value) {
                $parameters .= '/' . (!empty($key) ? $key . '/' : '') . $value;
            }
        }

        return $parameters;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function setParameters($parameters)
    {
        $this->_parameters = $parameters;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}