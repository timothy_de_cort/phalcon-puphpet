<?php

use Phalcon\Tag;

/**
 * Class Format
 */
class Format extends Tag
{
    /** @var string */
    protected $_valutaSign = '&euro;';

    /**
     * @return string
     */
    public function getValutaSign()
    {
        return '&nbsp;' . $this->_valutaSign;
    }

    /**
     * @param string $valutaSign
     */
    public function setValutaSign($valutaSign)
    {
        $this->_valutaSign = $valutaSign;
    }

    /**
     * @param float     $number
     * @param bool|true $showValutaSign
     * @param int       $digits
     * @return string
     */
    public function valuta($number, $showValutaSign = true, $digits = 2)
    {
        $value = number_format($number, $digits, ',', '.');
        return $value . ($showValutaSign ? $this->getValutaSign() : '');
    }

    /**
     * @param string $string
     * @param int    $maxLength
     * @param string $etc Add a few characters to the end of your string
     * @return string
     */
    public function string($string, $maxLength = 0, $etc = '')
    {
        $value = $string;
        if ($maxLength > 0 && strlen($string) > $maxLength) {
            $value = substr($string, 0, $maxLength) . $etc;
        }

        return $value;
    }

}