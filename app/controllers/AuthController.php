<?php

class AuthController extends DispatchController
{
    public function indexAction()
    {
        $this->redirect('auth/login');
    }

    public function loginAction()
    {
        $this->setTitle(Translator::find('CONNECT_ME'));

        $form = new LoginForm(null, ['login' => $this->getSession('login', true)]);

        if ($this->isValidFormSubmit($form, $this->request->getPost())) {

            $login    = $this->request->getPost('login');
            $password = $this->request->getPost('password');

            $user = new User();
            if ($user->validateLogin($login, $password, $user)) {
                LoginSession::setUser($user->getId());

                Messenger::flashInfo(Translator::find('WELCOME_USER', ['name' => $user->getName()]));
                $this->redirect('');
                return false;
            } else {
                Messenger::flashError(Translator::find('EMAIL_PASSWORD_COMBINATION_MISMATCH'));
            }
        }

        $this->view->setVar('form', $form);
    }

    public function logoutAction()
    {
        $name = LoginSession::getField('name');
        Messenger::flashInfo(Translator::find('SEE_YOU_NEXT_TIME_USER', ['name' => $name]));

        $this->destroySession('auth');
        $this->redirect('auth/login');
    }

    public function registerAction()
    {
        $form = new RegisterForm();

        if ($this->isValidFormSubmit($form, $this->request->getPost())) {

            $user = new User();
            $user->setName($this->request->getPost('name'));
            $user->setEmail($this->request->getPost('email'));
            $user->setPassword($this->request->getPost('password'));

            if ($user->create()) {
                $this->_sendSignupMail($user);
                $this->setSession('login', $this->request->getpost('email'));
                $this->redirect('auth/login');

            } else {
                Messenger::flashError($user->getMessages());
            }
        }

        $this->view->setVar('form', $form);
    }

    /**
     * Verify the response of the social authentication
     * This automatically redirects to AuthController::socialAction to complete the login process.
     */
    public function verifyAction()
    {
        Hybrid_Endpoint::process();
    }

    /**
     * Refer to http://hybridauth.sourceforge.net/userguide/Integrating_HybridAuth_SignIn.html
     * Code below refactored from the link above.
     *
     * @var string $type
     */
    public function socialAction($type = '')
    {
        $config     = HybridAuth::getConfig();
        $socialType = Oauth::OAUTH_DEFAULT;

        try {
            $hybridauth = new Hybrid_Auth($config);

            if ($this->hasSession('oauth_type')) {
                $socialPlatform = base64_decode($this->getSession('oauth_type'));
                $this->destroySession('oauth_type');
            } else {
                $socialPlatform = base64_decode($type);
                $this->setSession('oauth_type', $type);
            }

            /* Check if the provided social platform is present in our accepted networks */
            if (isset($config['providers'][$socialPlatform]) && $config['providers'][$socialPlatform]['enabled'] === true) {
                $socialType = constant('Oauth::OAUTH_' . strtoupper($socialPlatform));
            }

            $provider = $hybridauth->authenticate($socialPlatform);
            $profile  = $provider->getUserProfile();

            if (!$user = User::userExists($profile->identifier, $socialType)) {
                $user = new User();
                $user->setName($profile->displayName);
                $user->setEmail($profile->email);
                $user->setOauthId($socialType);
                $user->setOauthUid($profile->identifier);
                if (!$user->create()) {
                    Messenger::flashError(Translator::find('SOMETHING_WENT_WRONG_TRY_AGAIN'));
                    $this->redirect('auth/login');
                    return false;
                }

                /* Lets make sure the twitter handle is always up to date */
            } else if ($socialType == Oauth::OAUTH_TWITTER) {
                /** @var User $user */
                $user->setName($profile->displayName);
                $user->update();
            }

            LoginSession::setUser($user->getId());
            Messenger::flashInfo(Translator::find('WELCOME_USER', ['name' => $user->getName()]));

        } catch (Exception $e) {
            Messenger::flashError(Translator::find('SOMETHING_WENT_WRONG'));
            $this->redirect('auth/login');
            return false;
        }

        $this->redirect('');
    }


    /**
     * @param User   $user
     * @param string $subject
     */
    protected function _sendSignupMail(User $user, $subject = 'Registration')
    {
        $mailBuilder = new MailBuilder();
        $mailBuilder->setSubject($subject);
        $mailBuilder->setReceivers([$user->getName() => $user->getEmail()]);
        $mail = $mailBuilder->build(MailBuilder::MAIL_SIGNUP, ['user' => $user]);
        $mail->send();
    }
}
