<?php

use Phalcon\Mvc\View;

class DispatchController extends MainController
{
    /*
     * Order of execution:
     * 1. onConstruct
     * 2. beforeExecuteRoute
     * 3. initialize
     * 4. ?? ( your Action )
     * 5. afterExecuteRoute
     */

    /*
     * Executed even if Action is not found ( or user doesn't have access to Action )
     */
    public function onConstruct()
    {
    }

    /**
     * Execute code BEFORE Action is ran
     *
     * @param Phalcon\Mvc\Dispatcher $dispatcher
     * @return  bool
     */
    public function beforeExecuteRoute($dispatcher)
    {
    }

    /*
     * Executed right after $this->beforeExecuteRoute finishes with success.
     */
    public function initialize()
    {
        $this->view->setTemplateAfter('default');
    }

    /**
     * Execute code AFTER Action is ran
     *
     * @param Phalcon\Mvc\Dispatcher $dispatcher
     * @return  bool
     */
    public function afterExecuteRoute($dispatcher)
    {
        /* Set default title if none was set. */
        if (!$this->view->getVar('title')) {
            $this->setTitle(DefaultDi::get('config')->application->appName);
        }

        /* Add default prepend for header title */
        $this->tag->prependTitle('');

        /* Load LoginSession variables */
        if (LoginSession::exists()) {
            $this->view->setVar('user', LoginSession::getUser());
        }

        if ($this->request->isAjax()) {
            $this->plainViewRender();
        }

        /* Set translator */
        Translator::setLanguage($this->request->getBestLanguage());
        $this->view->setVar('t', Translator::getTranslation());
    }
}