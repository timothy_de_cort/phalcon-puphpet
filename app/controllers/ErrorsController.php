<?php
use Phalcon\Mvc\View;

class ErrorsController extends DispatchController
{
	public function initialize()
	{
		$this->setTitle('Oops!');
	}
	public function show404Action()
	{
	}
	public function show401Action()
	{
	}
	public function show500Action()
	{
        $this->plainViewRender();
	}
}