<?php

class UserController extends DispatchController
{
    public function indexAction()
    {
        $this->setTitle(Translator::find('PROFILE'));

        /** @var User $user */
        $user = User::findFirstById(LoginSession::getId());

        $this->view->setVar('userProfile', $user);
    }

    public function adminAction()
    {

    }
}