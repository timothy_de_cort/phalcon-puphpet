<?php

class IndexController extends DispatchController
{
    public function indexAction()
    {
        $this->setTitle('my index title');

        $this->view->setVar('hello', Translator::find('HELLO_WORLD'));

        Log::message('This log came from => IndexController::indexAction', Log::LOG_INFO);

    }
}