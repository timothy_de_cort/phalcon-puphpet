<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;

class MainController extends Controller
{
    /*
     * PHP version on production:  5.6.10
     * PHP version on development: 5.6.11
     *
     * php.ini
     * upload_max_filesize = 12M
     * post_max_size = 18M
     */

    /**
     * Do a HTTP redirection to another destination.
     * $destination = 'controller/action'; ( internal )
     * $destination = 'http://google.com'; ( external )
     *
     * @param string $destination
     * @param bool   $external
     * @param int    $statusCode
     */
    public function redirect($destination, $external = false, $statusCode = 302)
    {
        $this->view->disable();
        $this->response->redirect($destination, $external, $statusCode);
        return;
    }

    /**
     * Do a background, internal redirect. Execute another controller.
     * $controllerAction = [ 'controller' => 'dashboard', 'action' => 'index' ]
     *
     * @param string $destination
     */
    public function forward($destination)
    {
        $destination = explode('/', $destination);
        if (count($destination) == 1) {
            $destination[1] = 'index';
        }

        $this->dispatcher->forward(['controller' => $destination[0], 'action' => $destination[1]]);
        return;
    }

    /**
     * Convert date input to d-m-Y format
     *
     * @param string $date
     * @param string $format
     * @return string
     */
    public function setDMY($date, $format = 'd-m-Y')
    {
        return date($format, strtotime($date));
    }

    /**
     * Convert date input to Y-m-d format ( => database date format )
     *
     * @param string $date
     * @param string $format
     * @return string
     */
    public function setYMD($date, $format = 'Y-m-d')
    {
        return date($format, strtotime($date));
    }

    /**
     * Dump and array in a formated way. 'Die' if needed.
     *
     * @param array $array
     * @param bool  $die
     * @param bool  $return
     */
    public function dump($array = [], $die = false, $return = false)
    {
        echo '<pre>', print_r($array, $return), '</pre>';

        if ($die) {
            die;
        }
    }

    /**
     * @param string $pageTitle
     * @param string $headerTitle
     */
    public function setTitle($pageTitle = '', $headerTitle = '')
    {
        $this->view->setVar('title', $pageTitle);
        $this->tag->setTitle($pageTitle);

        if ($headerTitle != '') {
            $this->tag->setTitle($headerTitle);
        }
    }

    /**
     * @param Phalcon\Forms\Form $form
     * @param                    $postValues
     * @return bool
     */
    public function isValidFormSubmit($form, $postValues)
    {
        if (empty($postValues)) {
            $postValues = $this->request->getPost();
        }

        return $this->request->isPost() && $form->isValid($postValues);
    }

    /**
     * @param $index
     * @param $value
     */
    public function setSession($index, $value)
    {
        $this->session->set($index, $value);
    }

    /**
     * @param string $index
     * @param bool   $disposeValue
     * @return mixed
     */
    public function getSession($index, $disposeValue = false)
    {
        $value = $this->session->get($index);

        if ($disposeValue) {
            $this->destroySession($index);
        }

        return $value;
    }

    /**
     * @param $index
     * @return boolean
     */
    public function hasSession($index)
    {
        return $this->session->has($index);
    }

    /**
     * @param string $index
     */
    public function destroySession($index = '')
    {
        if (!empty($index)) {
            $this->session->remove($index);
        } else {
            $this->session->destroy();
        }
    }

    /*
     * Disable all the layers of the view except for the action_view
     */
    public function plainViewRender()
    {
        $this->view->disableLevel([
            View::LEVEL_BEFORE_TEMPLATE => true,
            View::LEVEL_LAYOUT          => true,
            View::LEVEL_AFTER_TEMPLATE  => true,
            View::LEVEL_MAIN_LAYOUT     => true,
        ]);
    }
}