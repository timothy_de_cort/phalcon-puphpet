<?php

class MysqlController extends DispatchController
{
    public function indexAction()
    {
        $sqlModel      = new SqlScriptLog();
        $insertedFiles = $sqlModel->getAllSqlScriptLogFileNames();

        $this->view->setVar('output', $this->_runScripts($insertedFiles));
        $this->plainViewRender();
    }

    /**
     * @param array  $insertedFiles
     * @param string $folder
     * @return array
     */
    protected function _runScripts($insertedFiles = [], $folder = '')
    {
        $dir    = new DirectoryIterator(DefaultDi::get('config')->application->mysqlDir . $folder);
        $done   = false;
        $output = [];

        foreach ($dir as $file) {
            if ($file->isFile() && $file->getExtension() == 'sql') {
                if (!in_array($file->getFileName(), $insertedFiles)) {
                    $query = file_get_contents($file->getPathName());

                    $done     = true;
                    $output[] = 'File: ' . $file->getFileName();

                    try {
                        $manager     = new Phalcon\Mvc\Model\Transaction\Manager();
                        $transaction = $manager->get();
                        $this->db->execute($query);
                        $transaction->commit();

                        $sqlScriptLog = new SqlScriptLog();
                        $sqlScriptLog->setName($file->getFileName());
                        $sqlScriptLog->save();

                        $output[]   = 'DONE';
                        $logMessage = 'SUCCESS: ' . $file->getFileName();
                        Log::message($logMessage, Log::LOG_MYSQL_SCRIPT);

                    } catch (Exception $e) {
                        $output[]   = $e->getMessage();
                        $logMessage = 'ERROR: ' . $file->getFileName() . ' ' . $e->getMessage();
                        Log::message($logMessage, Log::LOG_MYSQL_SCRIPT_ERROR);
                    }
                }
            }
        }

        if (!$done) {
            $output[] = 'Nothing to execute';
        }

        return $output;
    }
}
