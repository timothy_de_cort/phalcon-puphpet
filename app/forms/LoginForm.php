<?php

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends MainForm
{
    /**
     * @param object $entity
     * @param array  $options
     */
    public function initialize($entity, $options = [])
    {
        parent::initialize($entity, $options);

        /* Username */
        $login = new Text('login', ['class' => 'form-control']);
        $login->setLabel(Translator::find('EMAIL'));
        $login->setAttribute('Placeholder', '...');
        $login->addValidator(new PresenceOf(['message' => Translator::find('%ITEM%_IS_REQUIRED', ['ITEM' => Translator::find('EMAIL')])]));

        /* Password */
        $password = new Password('password', ['class' => 'form-control']);
        $password->setLabel(Translator::find('PASSWORD'));
        $password->setAttribute('Placeholder', '...');
        $password->addValidator(new PresenceOf(['message' => Translator::find('%ITEM%_IS_REQUIRED', ['ITEM' => Translator::find('PASSWORD')])]));

        if (isset($options['login']) && $options['login'] != '') {
            $login->setDefault($options['login']);
            $password->setAttribute('autofocus', 'autofocus');
        }

        $this->add($login);
        $this->add($password);

        /* Submit */
        $this->add(new Submit('submit', ['value' => Translator::find('LOGIN'), 'class' => 'btn btn-block btn-success']));
    }
}
