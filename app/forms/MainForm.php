<?php

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Forms\Element\Password;

class MainForm extends Phalcon\Forms\Form
{
    /**
     * @param object $entity
     * @param array  $options
     */
    public function initialize($entity, $options = [])
    {
        /** CSRF */
        $csrf = new Hidden('csrf');
        $this->add($csrf);
    }

    public function getCsrf()
    {
        return $this->security->getToken();
    }

    /**
     * @param string $name
     * @param bool   $showLabel
     */
    public function renderDecorated($name, $showLabel = true)
    {
        /** @var Phalcon\Forms\Element $element */
        $element       = $this->get($name);
        $messages      = $this->getMessagesFor($element->getName());
        $hasErrors     = count($messages) > 0;
        $classError    = '';
        $outputMessage = [];

        /* Check for errors */
        if ($hasErrors) {
            $classError = 'has-feedback has-error';

            foreach ($messages as $message) {
                $outputMessage[] = $message;
            }
        }

        /* Perform Magic */
        if (get_class($element) == 'Phalcon\Forms\Element\Check') {
            echo '<div class="form-group ' . $classError . '">';
            echo '<label for="' . $element->getName() . '">';
            echo $element . ucfirst($element->getLabel());
            echo '</label></div>';

        } else if (strpos($element->getAttribute('class'), 'datetime-picker') !== false) {
            echo '<div class="form-group ' . $classError . '">';
            if ($showLabel) {
                echo '<label for="' . $element->getName() . '">' . ucfirst($element->getLabel()) . '</label>';
            }
            echo '<div class="input-group date datetime-picker-container">';
            echo $element;
            echo '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div>';

        } else if (strpos($element->getAttribute('class'), 'date-picker') !== false) {
            echo '<div class="form-group ' . $classError . '">';
            if ($showLabel) {
                echo '<label for="' . $element->getName() . '">' . ucfirst($element->getLabel()) . '</label>';
            }
            echo '<div class="input-group date date-picker-container">';
            echo $element;
            echo '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div>';

        } else if (strpos($element->getAttribute('class'), 'color-picker') !== false) {
            echo '<div class="form-group ' . $classError . '">';
            if ($showLabel) {
                echo '<label for="' . $element->getName() . '">' . ucfirst($element->getLabel()) . '</label>';
            }
            echo '<div class="input-group color-picker-container">';
            echo $element;
            echo '<span class="input-group-addon"><i></i></span></div></div>';

        } else {
            echo '<div class="form-group ' . $classError . '">';
            if ($showLabel) {
                echo '<label for="' . $element->getName() . '">' . ucfirst($element->getLabel()) . '</label>';
            }
            echo $element;
            echo '</div>';
        }

        if ($hasErrors) {
            echo '<div class="form-group' . $classError . '">';
            echo '<div class="alert alert-danger alert-dismissible" role="alert">';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            echo implode('<br />', $outputMessage);
            echo '</div>';
            echo '</div>';
        }
    }

    public function renderDecoratedH($name)
    {
        /** @var Phalcon\Forms\Element $element */
        $element       = $this->get($name);
        $messages      = $this->getMessagesFor($element->getName());
        $hasErrors     = count($messages) > 0;
        $classError    = '';
        $outputMessage = [];

        /* Check for errors */
        if ($hasErrors) {
            $classError = 'has-feedback has-error';

            foreach ($messages as $message) {
                $outputMessage[] = $message;
            }
        }

        if (get_class($element) != 'Phalcon\Forms\Element\Check') {
            /* Perform Magic */
            echo '<div class="form-group ' . $classError . '">';
            echo '<label class="control-label uk-width-1-4" for="' . $element->getName() . '">' . ucfirst($element->getLabel()) . '</label>';

            echo '<div class="col-sm-8">';
            echo $element;
//        echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>';
            echo '</div>';
            echo '</div>';

        } else {
            /** Perform Magic */
            echo '<div class="form-group ' . $classError . '">';
            echo '<div class="col-sm-offset-4 col-sm-8">';

            echo '<div class="checkbox">';
            echo '<label>';
            echo $element;
            echo ucfirst($element->getLabel()) . '</label>';
//        echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>';
            echo '</div>';

            echo '</div>';
            echo '</div>';
        }

        if ($hasErrors) {
            echo '<div class="form-group' . $classError . '">';
            echo '<div class="col-sm-offset-4 col-sm-8">';

            echo '<div class="alert alert-danger alert-dismissible alert-bottomless" role="alert">';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            echo implode('<br />', $outputMessage) . '</div>';

            echo '</div>';
            echo '</div>';
        }
    }
}
