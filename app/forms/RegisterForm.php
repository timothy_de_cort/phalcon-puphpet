<?php

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Confirmation;

class RegisterForm extends MainForm
{
    /**
     * @param object $entity
     * @param array  $options
     */
    public function initialize($entity, $options = [])
    {
        parent::initialize($entity, $options);

        /* Username */
        $username = new Text('name', ['class' => 'form-control']);
        $username->setLabel(Translator::find('USERNAME'));
        $username->setAttribute('Placeholder', '...');
        $username->addValidator(new PresenceOf(['message' => Translator::find('%ITEM%_IS_REQUIRED', ['ITEM' => Translator::find('USERNAME')])]));
        $username->addValidator(new StringLength(['min' => 2, 'messageMinimum' => Translator::find('USERNAME_TOO_SHORT')]));
        $this->add($username);

        /* Email address */
        $email = new Email('email', ['class' => 'form-control']);
        $email->setLabel(Translator::find('EMAIL_ADDRESS'));
        $email->setAttribute('Placeholder', '...');
        $email->addValidator(new PresenceOf(['message' => Translator::find('%ITEM%_IS_REQUIRED', ['ITEM' => Translator::find('EMAIL_ADDRESS')])]));
        $email->addValidator(new EmailValidator(['message' => Translator::find('EMAIL_ADDRESS_NOT_VALID')]));
        $this->add($email);

        /* Password */
        $password = new Password('password', ['class' => 'form-control']);
        $password->setLabel(Translator::find('PASSWORD'));
        $password->addValidator(new PresenceOf(['message' => Translator::find('%ITEM%_IS_REQUIRED', ['ITEM' => Translator::find('PASSWORD')])]));
        $password->addValidator(new StringLength(['min' => 6, 'messageMinimum' => Translator::find('PASSWORD_TOO_SHORT')]));
        $this->add($password);

        /* Password confirm */
        $passwordConfirm = new Password('password_confirm', ['class' => 'form-control']);
        $passwordConfirm->setLabel(Translator::find('PASSWORD_CONFIRM'));
        $passwordConfirm->addValidator(new Confirmation(['message' => Translator::find('PASSWORD_DONT_MATCH'), 'with' => 'password']));
        $this->add($passwordConfirm);

        /* Submit */
        $this->add(new Submit('submit', ['value' => Translator::find('REGISTER'), 'class' => 'btn btn-success']));
    }
}
