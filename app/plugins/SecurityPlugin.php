<?php
use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatcherException;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{
    /**
     * Returns an existing or new access control list
     *
     * @returns AclList
     */
    protected static function _getAcl()
    {
        $acl     = new AclList();
        $aclFile = APP_PATH . DefaultDi::get('config')->application->aclFile;

        if (!is_file($aclFile)) {
            $acl->setDefaultAction(Acl::DENY);

            /* Register roles */
            $rolePublic = new Role('public');
            $roleUser   = new Role('user');
            $roleAdmin  = new Role('admin');

            /* Add role inheritance */
            $acl->addRole($rolePublic);
            $acl->addRole($roleUser);
            $acl->addRole($roleAdmin, $roleUser);

            //Public area resources
            $publicResources = [
                'index'  => ['index'],
                'auth'   => ['index', 'login', 'register'],
                'errors' => ['show404', 'show401', 'show500'],
            ];

            //Private area resources
            $privateResources = [
                'index'  => ['index'],
                'errors' => ['show404', 'show401', 'show500'],
                'auth'   => ['logout'],
                'user'   => ['index'],
            ];

            //Admin area resources
            $adminResources = [
                'mysql' => ['index'],
            ];


            foreach ($publicResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            foreach ($privateResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            foreach ($adminResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            /* Grant access to public areas */
            foreach ($publicResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow('public', $resource, $action);
                }
            }

            /* Grant access to public areas */
            foreach ($privateResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow('user', $resource, $action);
                }
            }

            /* Grant access to admin resources */
            foreach ($adminResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow('admin', $resource, $action);
                }
            }

            /* save the acl to file */
            file_put_contents($aclFile, serialize($acl));

        } else {
            /* retrieve the acl from file */
            $acl = unserialize(file_get_contents($aclFile));
        }

        return $acl;
    }

    /**
     * This action is executed before execute any action in the application
     *
     * @param Event      $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();

        $allowed = self::isAllowed($controller, $action);
        if ($allowed != Acl::ALLOW) {
            if ($allowed != Acl::DENY) {
                $dispatcher->forward([
                    'controller' => 'errors',
                    'action'     => 'show404'
                ]);
            } else {
                $dispatcher->forward([
                    'controller' => 'index',
                    'action'     => 'index'
                ]);
            }
            return false;
        }
    }

    /**
     * @param string $controller
     * @param string $action
     * @return mixed
     */
    public static function isAllowed($controller, $action)
    {
        // set default 'index' resource | action
        $controller = empty($controller) ? 'index' : str_ireplace('-', '', $controller);
        $action     = empty($action) ? 'index' : str_ireplace('-', '', $action);

        $acl  = self::_getAcl();
        $role = LoginSession::getACLrole('role');

        return $acl->isAllowed($role, $controller, $action);
    }
}