<?php
use Phalcon\Config\Adapter\Ini as ConfigIni;

$config = new ConfigIni(APP_PATH . 'app/config/config.ini');

switch (APP_ENV) {
    case 'dev':
        $config->merge(new Phalcon\Config\Adapter\Ini(APP_PATH . 'app/config/config_development.ini'));
        break;
    case 'testing':
        $config->merge(new Phalcon\Config\Adapter\Ini(APP_PATH . 'app/config/config_test.ini'));
        break;
    case 'production':
        $config->merge(new Phalcon\Config\Adapter\Ini(APP_PATH . 'app/config/config_production.ini'));
        break;
}