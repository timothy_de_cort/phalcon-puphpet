<?php

$router = new Phalcon\Mvc\Router();

$router->add('/([a-zA-Z\-]+)/([a-zA-Z\-]+)/:params', [
    'controller' => 1,
    'action'     => 2,
    'params'     => 3
])->convert('action', function ($action) {
    return lcfirst(Phalcon\Text::lower(Phalcon\Text::camelize($action)));
});

return $router;