<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/** The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework */
$di = new FactoryDefault();

/** Make the config available in the app */
$di->set('config', $config);

/** We register the events manager */
$di->set('dispatcher', function () use ($di, $config) {
    $eventsManager = new EventsManager;

    /** Check if the user is allowed to access certain action using the SecurityPlugin */
    $eventsManager->attach('dispatch:beforeDispatch', new SecurityPlugin);

    /** Handle exceptions and not-found exceptions using NotFoundPlugin */
    if (APP_ENV != $config->application->environment) {
        $eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);
    }

    $dispatcher = new Dispatcher;
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});

$di->set('router', function () use ($config) {
    return include APP_PATH . 'app/bootstrap/routes.php';
}, true);

$di->set('url', function () use ($config) {
    $url = new UrlProvider();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

$di->set('view', function () use ($config) {
    $view = new View();
    $view->setViewsDir(APP_PATH . $config->application->viewsDir);
    $view->setPartialsDir($config->application->partialsDir);
    return $view;
});

$di->set('db', function () use ($config) {
    $dbclass = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    return new $dbclass([
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->name
    ]);
});

$di->set('modelsMetadata', function () {
    return new MetaData();
});

$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

$di->set('flashSession', function () {
    return new FlashSession([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'warning' => 'alert alert-warning',
        'notice'  => 'alert alert-info',
    ]);
});

$di->set('crypt', function () use ($config) {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey($config->application->cryptKey);
    return $crypt;
});

$di->set('security', function () {
    $security = new Phalcon\Security();
    $security->setWorkFactor(11);
    return $security;
}, true);

$di->set('link', function () {
    return new Link();
});

$di->set('format', function () {
    return new Format();
});
