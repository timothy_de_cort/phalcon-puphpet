<?php

namespace Demo\Model;

class Mail extends \MainModel
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $user_id;
    /** @var bool */
    protected $sent;
    /** @var string */
    protected $from;
    /** @var string */
    protected $to;
    /** @var string */
    protected $date;
    /** @var string */
    protected $subject;
    /** @var string */
    protected $body;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('user_id', 'User', 'id');
    }

    public function beforeValidationOnCreate()
    {
        if (\LoginSession::exists()) {
            $this->setUserId(\LoginSession::getId());
        }
    }

    /**
     * @return array
     */
    public function columnMap()
    {
        return [
            'id'      => 'id',
            'user_id' => 'user_id',
            'sent'    => 'sent',
            'from'    => 'from',
            'to'      => 'to',
            'date'    => 'date',
            'subject' => 'subject',
            'body'    => 'body',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return boolean
     */
    public function isSent()
    {
        return $this->sent;
    }

    /**
     * @param boolean $sent
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }
}