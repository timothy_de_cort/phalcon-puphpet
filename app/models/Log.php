<?php

use Phalcon\Mvc\Model\Relation;

class Log extends MainModel
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $user_id;
    /** @var string */
    protected $type;
    /** @var string */
    protected $url;
    /** @var string */
    protected $content;
    /** @var string */
    protected $ip;
    /** @var string */
    protected $date;

    const LOG_INFO = 'LOG_INFO';
    const LOG_ERROR = 'LOG_ERROR';
    const LOG_REGISTER = 'LOG_REGISTER';
    const LOG_FIRST_LOGIN = 'LOG_FIRST_LOGIN';
    const LOG_IMPORT_USER = 'LOG_IMPORT_USER';
    const LOG_MYSQL_SCRIPT = 'LOG_MYSQL_SCRIPT';
    const LOG_NOT_TRANSLATED = 'LOG_NOT_TRANSLATED';
    const LOG_REGISTER_ERROR = 'LOG_REGISTER_ERROR';
    const LOG_MYSQL_SCRIPT_ERROR = 'LOG_MYSQL_SCRIPT_ERROR';

    public static function message($content, $type = Log::LOG_INFO, $userId = null)
    {
        $log = new Log();
        $log->setUserId(LoginSession::exists() ? LoginSession::getId() : $userId);
        $log->setType($type);
        $log->setUrl(DefaultDi::get('request')->getUri());
        $log->setContent($content);
        $log->setIp(DefaultDi::get('request')->getClientAddress());
        $log->create();
    }

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('user_id', 'User', 'id');
    }

    public function columnMap()
    {
        return [
            'id'      => 'id',
            'user_id' => 'user_id',
            'type'    => 'type',
            'url'     => 'url',
            'content' => 'content',
            'ip'      => 'ip',
            'date'    => 'date',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}