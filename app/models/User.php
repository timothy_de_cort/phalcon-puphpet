<?php

use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class User extends MainModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $name;
    /** @var string */
    protected $email;
    /** @var string */
    protected $password;
    /** @var int */
    protected $oauth_id;
    /** @var int */
    protected $oauth_uid;
    /** @var int */
    protected $role_id;

    /**
     * @param string $login
     * @param string $password
     * @param Phalcon\Mvc\Model
     * @return bool|User
     */
    public function validateLogin($login, $password, &$user)
    {
        /** @var User $user */
        $user = User::findFirst(['email = :login:', 'bind' => ['login' => $login]]);

        if ($this->validEncryption($password, $user->password)) {
            return $user;
        }

        return false;
    }

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('oauth_id', 'Oauth', 'id', [
            'foreignKey' => ['message' => 'Oauth cannot be deleted because users are using it.']
        ]);

        $this->hasMany('id', 'Log', 'user_id', ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]);
        $this->hasMany('id', 'Mail', 'user_id', ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]);
    }

    public function columnMap()
    {
        return [
            'id'        => 'id',
            'name'      => 'name',
            'email'     => 'email',
            'password'  => 'password',
            'oauth_id'  => 'oauth_id',
            'oauth_uid' => 'oauth_uid',
            'role_id'   => 'role_id',
        ];
    }

    public function beforeValidationOnCreate()
    {
        if (empty($this->role_id)) {
            $this->role_id = Role::ROLE_PUBLIC;
        }
        $this->password = $this->setEncryption($this->password);
    }

    /**
     * @return bool
     */
    public function validation()
    {
        if ($this->email != '') {
            $this->validate(new EmailValidator([
                'field' => 'email'
            ]));

            $this->validate(new UniquenessValidator([
                'field'   => 'email',
                'message' => 'Sorry, ' . $this->email . ' was registered by another user.'
            ]));
        }

        $this->validate(new UniquenessValidator([
            'field'   => 'name',
            'message' => 'Sorry, ' . $this->name . ' is already taken.'
        ]));

        return $this->validationHasFailed() != true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getOauthId()
    {
        return $this->oauth_id;
    }

    /**
     * @param int $oauth_id
     */
    public function setOauthId($oauth_id)
    {
        $this->oauth_id = $oauth_id;
    }

    /**
     * @return int
     */
    public function getOauthUid()
    {
        return $this->oauth_uid;
    }

    /**
     * @param int $oauth_uid
     */
    public function setOauthUid($oauth_uid)
    {
        $this->oauth_uid = $oauth_uid;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param int $role_id
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;
    }

    /**
     * @return array
     */
    public function getSessionArray()
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'email'     => $this->email,
            'role_id'   => $this->role_id,
            'oauth_id'  => $this->oauth_id,
            'oauth_uid' => $this->oauth_uid,
            'role'      => Role::findFirstById($this->role_id)->getName(),
        ];
    }
}