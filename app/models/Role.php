<?php

use Phalcon\Mvc\Model\Relation;

class Role extends MainModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $name;
    /** @var string */
    protected $label;

    const ROLE_ADMIN = 1;
    const ROLE_BASTARD = 2;
    const ROLE_GUEST = 3;
    const ROLE_PUBLIC = 4;

    const DEFAULT_ROLE_NAME = 'public';


    public function initialize()
    {
        parent::initialize();

        $this->hasMany('id', 'User', 'role_id', ['foreignKey' => ['action' => Relation::ACTION_RESTRICT]]);
    }

    public function columnMap()
    {
        return [
            'id'    => 'id',
            'name'  => 'name',
            'label' => 'label',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
