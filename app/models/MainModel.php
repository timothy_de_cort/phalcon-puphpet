<?php

use Phalcon\Mvc\Model;

class MainModel extends Model
{
    public function initialize()
    {
        /* phqlLiterals: Disallow using parameters directly into queries. Force Bound Parameters. */
        /* exceptionOnFailedSave: Show errors on failed save instead of returning FALSE. */
        Model::setup(['phqlLiterals' => true, 'exceptionOnFailedSave' => APP_ENV != 'development']);

        /* Only update changed columns, might improve performance ( specially when table has blob/text columns ) */
//        $this->useDynamicUpdate(true);
    }

    /**
     * @param string $value
     * @return string
     */
    public function setEncryption($value)
    {
        /** @var Phalcon\Security $securityComponent */
        $securityComponent = $this->getDI()->get('security');

        return $securityComponent->hash($value);
    }

    /**
     * @param string $value
     * @param string $compareTo
     * @return bool
     */
    public function validEncryption($value, $compareTo)
    {
        /** @var Phalcon\Security $securityComponent */
        $securityComponent = $this->getDI()->get('security');

        return $securityComponent->checkHash($value, $compareTo);
    }

    public function notSave()
    {
        if (APP_ENV == DefaultDi::get('config')->application->environment) {
            Messenger::flashError($this->getMessages());
        }
    }

    /**
     * Convert date input to d-m-Y format
     *
     * @param string $full_date
     * @param string $format
     * @return string
     */
    public function setDMY($full_date, $format = 'd-m-Y')
    {
        $date = [$full_date, ''];
        if (strpos($date, ' ') > -1) {
            $date = explode(' ', $full_date);
        }
        return date($format, strtotime($date[0])) . $date[1];
    }

    /**
     * Convert date input to Y-m-d format ( => database date format )
     *
     * @param string $date
     * @param string $format
     * @return string
     */
    public function setYMD($date, $format = 'Y-m-d')
    {
        return date($format, strtotime($date));
    }
}