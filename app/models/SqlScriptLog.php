<?php

use Phalcon\Mvc\Model\Resultset;

class SqlScriptLog extends MainModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $date;
    /** @var string */
    protected $name;

    public function columnMap()
    {
        return [
            'id'   => 'id',
            'date' => 'date',
            'name' => 'name',
        ];
    }

    /**
     * @return array
     */
    public function getAllSqlScriptLogFileNames()
    {
        $scripts = SqlScriptLog::find(['columns' => 'name', 'order' => 'name']);

        $names = [];
        foreach ($scripts as $script) {
            $names[] = $script['name'];
        }
        return $names;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
