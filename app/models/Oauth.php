<?php

use Phalcon\Mvc\Model\Relation;

class Oauth extends MainModel
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $name;

    const OAUTH_DEFAULT = 1;
    const OAUTH_FACEBOOK = 2;
    const OAUTH_TWITTER = 3;

    public function initialize()
    {
        parent::initialize();

        $this->hasMany('id', 'User', 'oauth_id', ['foreignKey' => ['action' => Relation::ACTION_CASCADE]]);
    }

    public function columnMap()
    {
        return [
            'id'   => 'id',
            'name' => 'name',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
