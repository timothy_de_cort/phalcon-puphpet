<?php

use Phalcon\Mvc\User\Component;
use Phalcon\Session\Bag as SessionBag;

class LoginSession extends Component
{
    /** @var Phalcon\Session\Bag */
    protected static $_auth = null;
    /** @var string */
    protected static $_sessionBag = 'auth';
    /** @var string */
    protected static $_sessionName = 'user';

    /**
     * @return array
     */
    protected static function _getInstance()
    {
        if (!self::$_auth) {
            self::$_auth = new SessionBag(self::$_sessionBag);
        }

        return is_array(self::$_auth->get(self::$_sessionName)) ? self::$_auth->get(self::$_sessionName) : [];
    }

    /**
     * @return bool
     */
    public static function exists()
    {
        return self::_getInstance() != null;
    }

    /**
     * @param $field
     * @return mixed
     * @throws Exception
     */
    public static function getField($field)
    {
        if (array_key_exists($field, self::getUser())) {
            return self::getUser()[$field];
        } else {
            if (LoginSession::exists()) {
                Log::message('"' . $field . '" not found in session array', Log::LOG_ERROR);
            }
            return null;
        }
    }

    /**
     * @return string
     */
    public static function getACLrole()
    {
        return self::getField('role') ? self::getField('role') : Role::DEFAULT_ROLE_NAME;
    }

    /**
     * @return bool
     */
    public static function isAdmin()
    {
        return self::getField('role_id') == Role::ROLE_ADMIN;
    }

    /**
     * @return int
     */
    public static function getId()
    {
        return self::getField('id');
    }

    /**
     * @return array
     */
    public static function getUser()
    {
        return self::_getInstance();
    }

    /**
     * @param int $id
     */
    public static function setUser($id)
    {
        if (!self::$_auth) {
            self::$_auth = new \Phalcon\Session\Bag(self::$_sessionBag);
        }

        $userArray = User::findFirstById($id)->getSessionArray();

        self::$_auth->set(self::$_sessionName, $userArray);
    }

    /**
     * @param string $field
     * @param string $value
     */
    public static function setField($field, $value)
    {
        $userArray         = self::_getInstance();
        $userArray[$field] = $value;
        self::$_auth->set(self::$_sessionName, $userArray);
    }

    public static function destroy()
    {
        DefaultDi::get('session')->remove(self::$_sessionBag);
    }
}