<?php

use Phalcon\Mvc\User\Component;

class DefaultDi extends Component
{
    /**
     * @param $class
     * @return mixed
     */
    public static function get($class)
    {
        return Phalcon\Di::getDefault()->get($class);
    }
}