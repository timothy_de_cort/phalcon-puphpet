<?php

use Phalcon\Mvc\User\Component;
use Phalcon\Translate\Adapter\NativeArray;

class Translator extends Component
{
    /** @var string */
    protected static $language = '';

    const DEFAULT_LANGUAGE = 'en';

    /**
     * @param string $language
     */
    public static function setLanguage($language)
    {
        self::$language = $language;
    }

    /**
     * @return string
     */
    public static function getLanguage()
    {
        return self::$language != '' ? self::$language : Translator::DEFAULT_LANGUAGE;
    }

    /**
     * @param string $translationKey
     * @param array  $placeholder
     * @return string
     */
    public static function find($translationKey, $placeholder = [])
    {
        if (!self::getTranslation()->exists($translationKey)) {
            Log::message($translationKey, Log::LOG_NOT_TRANSLATED);
        }

        return self::getTranslation()->_($translationKey, $placeholder);
    }

    /**
     * @return NativeArray
     */
    public static function getTranslation()
    {
        $language = self::getLanguage();
        $language = strpos($language, '-') == -1 ? $language : explode('-', $language)[0];

        $translations    = [];
        $licalizationDir = APP_PATH . DefaultDi::get('config')->application->localizationDir;

        if (file_exists($licalizationDir . $language . '.php')) {
            require $licalizationDir . $language . '.php';

        } else {
            require $licalizationDir . self::DEFAULT_LANGUAGE . '.php';
        }

        return new NativeArray(['content' => $translations]);
    }
}