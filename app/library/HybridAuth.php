<?php

use Phalcon\Mvc\User\Component;

class HybridAuth extends Component
{
    /**
     * @return array
     */
    public static function getConfig()
    {
        return [
            'base_url'   => DefaultDi::get('config')->application->homeUrl . '/auth/verify',
            'providers'  => [
                // openid providers
                'OpenID'     => [
                    'enabled' => false
                ],
                'Yahoo'      => [
                    'enabled' => false,
                    'keys'    => ['key' => '', 'secret' => ''],
                ],
                'AOL'        => [
                    'enabled' => false
                ],
                'Google'     => [
                    'enabled' => false,
                    'keys'    => ['id' => '', 'secret' => ''],
                ],
                'Facebook'   => [
                    'enabled'        => true,
                    'keys'           => [
                        'id'     => DefaultDi::get('config')->social->facebookId,
                        'secret' => DefaultDi::get('config')->social->facebookSecret,
                    ],
                    'scope'          => 'email, user_birthday',
                    'wrapper'        => ['class' => 'HybridAuth_Facebook'],
                    'trustForwarded' => true,
                ],
                'Twitter'    => [
                    'enabled' => false,
                    'keys'    => [
                        'key'    => '',
                        'secret' => '',
                    ]
                ],
                // windows live
                'Live'       => [
                    'enabled' => false,
                    'keys'    => ['id' => '', 'secret' => '']
                ],
                'LinkedIn'   => [
                    'enabled' => false,
                    'keys'    => ['key' => '', 'secret' => '']
                ],
                'Foursquare' => [
                    'enabled' => false,
                    'keys'    => ['id' => '', 'secret' => '']
                ],
            ],
            // If you want to enable logging, set 'debug_mode' to true.
            // You can also set it to
            // - 'error' To log only error messages. Useful in production
            // - 'info' To log info and error messages (ignore debug messages)
            'debug_mode' => false,
            // Path to file writable by the web server. Required if 'debug_mode' is not false
//            'debug_file' => '',
            'debug_file' => APP_PATH . 'public/debug.txt',
        ];
    }
}