<?php

use Phalcon\Mvc\User\Component;

class MenuBuilder extends Component
{
    protected $_activeMenuItemClass = 'active';

    protected $_controllerName = '';
    protected $_actionName = '';

    /**
     * @param string $location
     */
    public function getMenu($location = 'left')
    {
        $xmlMenu = simplexml_load_file(APP_PATH . DefaultDi::get('config')->application->navigationFile);

        $this->_controllerName = $this->view->getControllerName();
        $this->_actionName     = $this->view->getActionName();

        foreach ($xmlMenu as $menu) {
            // set default location
            $menuLocation = $menu['location']->__toString();

            $isModal = $menu['type'] && $menu['type']->__toString() == 'modal';

            // skip if not allowed by ACL
            if (!SecurityPlugin::isAllowed($menu->controller, $menu->action)) {
                if (!$isModal) {
                    continue;
                }
            }

            // skip if not the right location
            if ($menuLocation != $location) {
                continue;
            }

            $this->_generateMenuItem($menu);
        }
    }

    /**
     * @param $menu
     * @return bool
     */
    protected function _generateMenuItem($menu)
    {
        $isModal = $menu['type'] && $menu['type']->__toString() == 'modal';

        if (!SecurityPlugin::isAllowed($menu->controller, $menu->action)) {
            if (!$isModal) {
                return false;
            }
        }

        // configure label
        switch ($menu->label['source']) {
            case 'session':
                $label = LoginSession::getField($menu->label->__toString() ?: '');
                break;
            default:
                $label = $menu->label->__toString() ? Translator::find($menu->label->__toString()) : '';
                break;
        }

        // add icon
        if ($menu->icon) {
            $icon = '<i class="' . $menu->icon . '"></i>';

            if (empty($label)) {
                $label = $icon;

            } else {
                switch ($menu->icon['position']) {
                    case 'after':
                        $label = $label . '&nbsp;' . $icon;
                        break;
                    case 'before':
                    default:
                        $label = $icon . '&nbsp;' . $label;
                        break;
                }
            }
        }

        // pre-magic the properties
        $properties = '';
        if ($menu->properties) {
            foreach ($menu->properties->attributes() as $property => $value) {
                $properties .= ' ' . $property . '="' . $value . '"';
            }
        }

        // pre-magic the parameters
        $parameters = '';
        if ($menu->parameters) {
            foreach ($menu->parameters->attributes() as $property => $value) {
                $parameters .= '/' . $property . '/' . $value;
            }
        }

        // check if active
        $isActive = $this->_controllerName == $menu->controller && $this->_actionName == $menu->action;

        // pre-magic the prefix
        $prefix     = isset($menu['prefix']) ? $menu['prefix']->__toString() : '';
        $hasSubmenu = $menu->submenu ? 'has-submenu' : '';
        $hasClass   = $menu['class'] ?: '';
        $class      = $hasClass . ' ' . $hasSubmenu . ($isActive ? ' ' . $this->getActiveMenuItemClass() : '');
        $url        = isset($menu['url']) ? $menu['url']->__toString() : '';

        echo '<li class="' . trim($class) . '">';
        echo $this->link->setController($menu->controller->__toString())->setAction($menu->action->__toString())->setUrl($url)
            ->setPrefix($prefix)->setLabel($label)->setProperties($properties)->setParameters($parameters)->setModal($isModal);

        if ($menu->submenu) {
            echo PHP_EOL . '<ul class="submenu menu vertical" data-submenu>' . PHP_EOL;
            foreach ($menu->submenu->children() as $submenu) {
                $this->_generateMenuItem($submenu);
            }
            echo '</ul>' . PHP_EOL;
        }

        echo '</li>' . PHP_EOL;
    }

    /**
     * @return string
     */
    public function getActiveMenuItemClass()
    {
        return $this->_activeMenuItemClass;
    }

    /**
     * @param string $activeMenuItemClass
     */
    public function setActiveMenuItemClass($activeMenuItemClass)
    {
        $this->_activeMenuItemClass = $activeMenuItemClass;
    }
}