<?php
require_once(APP_PATH . DefaultDi::get('config')->application->vendorDir . 'PHPMailer/PHPMailerAutoload.php');
require_once(APP_PATH . DefaultDi::get('config')->application->modelsDir . 'Mail.php');
use Demo\Model;

class Mail extends \PHPMailer
{
    public $Mailer;
    public $Host;
    public $Port;

    public function __construct($exceptions = false)
    {
        parent::__construct($exceptions);

        $this->Mailer = DefaultDi::get('config')->mail->protocol;
        $this->Host = DefaultDi::get('config')->mail->host;
        $this->Port = DefaultDi::get('config')->mail->port;
        $this->IsHTML(true);
    }

    /**
     * @return bool
     * @throws Exception
     * @throws phpmailerException
     */
    public function send()
    {
        $result = parent::send();

        $this->save($result);

        return $result;
    }

    /**
     * @param bool $sent
     * @return bool
     */
    protected function save($sent = false)
    {
        $mail = new \Demo\Model\Mail();
        $mail->setSent($sent);
        $mail->setTo(implode(', ', array_keys($this->getAllRecipientAddresses())));
        $mail->setFrom($this->From);
        $mail->setBody($this->Body);
        $mail->setSubject($this->Subject);

        return $mail->create();
    }
}