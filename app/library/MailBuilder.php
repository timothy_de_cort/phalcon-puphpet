<?php

use Phalcon\Mvc\User\Component;

class MailBuilder extends Component
{
    /** @var string */
    protected $_sender;
    /** @var string */
    protected $_senderName;
    /** @var array */
    protected $_receivers;
    /** @var array */
    protected $_cc;
    /** @var array */
    protected $_bcc;
    /** @var string */
    protected $_subject;

    const MAIL_SIGNUP = 'MAIL_SIGNUP';

    /**
     * @param string $type
     * @param array  $params
     * @param string $baseTemplate
     * @return Mail
     */
    public function build($type, $params = [], $baseTemplate = 'default_mail')
    {
        $mail = new Mail();

        /* set subject */
        $mail->Subject = $this->getSubject();

        /* set sender */
        $mail->From     = $this->getSender();
        $mail->FromName = $this->getSenderName();

        /* set receivers */
        foreach ($this->getReceivers() as $name => $emailAddress) {
            $mail->addAddress($emailAddress, !is_numeric($name) ? $name : '');
        }

        /* set cc */
        if ($this->getCc()) {
            foreach ($this->getCc() as $name => $emailAddress) {
                $mail->addCC($emailAddress, !is_numeric($name) ? $name : '');
            }
        }

        /* set bcc */
        if ($this->getBcc()) {
            foreach ($this->getBcc() as $name => $emailAddress) {
                $mail->addBCC($emailAddress, !is_numeric($name) ? $name : '');
            }
        }

        /* set parameters */
        $this->_setDefaultParameters($params);
        $mail->Body = $this->_getBody($this->_getTemplate($type), $params, $baseTemplate);

        return $mail;
    }

    /**
     * @param $params
     */
    protected function _setDefaultParameters(&$params)
    {
        $params = array_merge($params, [
            'homeUrl' => DefaultDi::get('config')->application->homeUrl,
        ]);
    }

    /**
     * @param string $type
     * @return string
     */
    protected function _getTemplate($type)
    {
        switch ($type) {
            case self::MAIL_SIGNUP:
            default:
                return 'signup';
        }
    }

    /**
     * @param string $template
     * @param array  $params
     * @param string $baseTemplate
     * @return string
     */
    protected function _getBody($template, array $params = [], $baseTemplate)
    {
        $body = DefaultDi::get('view')->setLayout($baseTemplate)->getRender('mail', $template, $params);

        return $body;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        if (!$this->_sender) {
            $this->_sender = DefaultDi::get('config')->mail->defaultSender;
        }

        return $this->_sender;
    }

    /**
     * @param string $sender
     */
    public function setSender($sender)
    {
        if (is_array($sender)) {
            foreach ($sender as $name => $email) {
                $this->_sender     = $email;
                $this->_senderName = $name;
            }

        } else {
            $this->_sender     = DefaultDi::get('config')->mail->defaultSender;
            $this->_senderName = DefaultDi::get('config')->mail->defaultSenderName;
        }
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        if (!$this->_senderName) {
            $this->_senderName = DefaultDi::get('config')->mail->defaultSenderName;
        }

        return $this->_senderName;
    }

    /**
     * @param string $senderName
     */
    public function setSenderName($senderName)
    {
        $this->_senderName = $senderName;
    }

    /**
     * @return array
     */
    public function getReceivers()
    {
        return $this->_receivers;
    }

    /**
     * @param array $receivers
     */
    public function setReceivers($receivers)
    {
        $this->_receivers = $receivers;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;
    }

    /**
     * @return array
     */
    public function getCc()
    {
        return $this->_cc;
    }

    /**
     * @param array $cc
     */
    public function setCc($cc)
    {
        $this->_cc = $cc;
    }

    /**
     * @return array
     */
    public function getBcc()
    {
        return $this->_bcc;
    }

    /**
     * @param array $bcc
     */
    public function setBcc($bcc)
    {
        $this->_bcc = $bcc;
    }
}