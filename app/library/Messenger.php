<?php

use Phalcon\Mvc\User\Component;

class Messenger extends Component
{
    protected static $_messageCloseHtml = '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>';

    /**
     * @param array|string $messages
     */
    public static function flashError($messages)
    {
        $messages = !is_array($messages) ? [$messages] : $messages;

        foreach ($messages as $message) {
            DefaultDi::get('flashSession')->error(self::_getBody($message));
        }
    }

    /**
     * @param array|string $messages
     */
    public static function flashWarning($messages)
    {
        $messages = !is_array($messages) ? [$messages] : $messages;

        foreach ($messages as $message) {
            DefaultDi::get('flashSession')->warning(self::_getBody($message));
        }
    }

    /**
     * @param array|string $messages
     */
    public static function flashInfo($messages)
    {
        $messages = !is_array($messages) ? [$messages] : $messages;

        foreach ($messages as $message) {
            DefaultDi::get('flashSession')->notice(self::_getBody($message));
        }
    }

    /**
     * @param array|string $messages
     */
    public static function flashSuccess($messages)
    {
        $messages = !is_array($messages) ? [$messages] : $messages;

        foreach ($messages as $message) {
            DefaultDi::get('flashSession')->success(self::_getBody($message));
        }
    }

    /**
     * @param string $message
     * @return string
     */
    protected static function _getBody($message)
    {
        return self::$_messageCloseHtml . $message;
    }
}