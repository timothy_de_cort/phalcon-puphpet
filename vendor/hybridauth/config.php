<?php
/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$config = [
    "base_url"   => $this->config->application->homeUrl . "auth/verify",
    "providers"  => [
        // openid providers
        "OpenID"     => [
            "enabled" => false
        ],
        "Yahoo"      => [
            "enabled" => false,
            "keys"    => ["key" => "", "secret" => ""],
        ],
        "AOL"        => [
            "enabled" => false
        ],
        "Google"     => [
            "enabled" => false,
            "keys"    => ["id" => "", "secret" => ""],
        ],
        "Facebook"   => [
            "enabled"        => false,
            "keys"           => ["id" => "", "secret" => ""],
            "trustForwarded" => false
        ],
        "Twitter"    => [
            "enabled" => false,
            "keys"    => [
                "key"    => $this->config->social->twitterKey,
                "secret" => $this->config->social->twitterSecret
            ]
        ],
        // windows live
        "Live"       => [
            "enabled" => false,
            "keys"    => ["id" => "", "secret" => ""]
        ],
        "LinkedIn"   => [
            "enabled" => false,
            "keys"    => ["key" => "", "secret" => ""]
        ],
        "Foursquare" => [
            "enabled" => false,
            "keys"    => ["id" => "", "secret" => ""]
        ],
    ],
    // If you want to enable logging, set 'debug_mode' to true.
    // You can also set it to
    // - "error" To log only error messages. Useful in production
    // - "info" To log info and error messages (ignore debug messages)
    "debug_mode" => false,
    // Path to file writable by the web server. Required if 'debug_mode' is not false
    "debug_file" => APP_PATH . 'public/debug.txt',
];
