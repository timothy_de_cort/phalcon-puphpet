#!/bin/bash

set -o pipefail

# find current location.
path=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
appPath=${path}/../../../
mysqlDir=${appPath}/schemas/init/
printLength=50
debugLog=/tmp/debugLog

tmpMyCnf=/tmp/.my.cnf > ${tmpMyCnf}
echo "[client]" >> ${tmpMyCnf}
echo "user=demo" >> ${tmpMyCnf}
echo "password=demo" >> ${tmpMyCnf}
echo "database=demo" >> ${tmpMyCnf}


> ${debugLog}

fgred=""
fggrn=""
normal=""

[ -t 1 ] && fgred="$(tput setaf 1)" # Red
[ -t 1 ] && fggrn="$(tput setaf 2)" # Green
[ -t 1 ] && normal=$(tput sgr0)

spinner()
{
    local pid=$!
    local text=$1
    local delay=0.1
    local spinstr='-\|/'
    while [ "$(ps a | awk '{print $1}' | grep ${pid})" ]; do
        local temp=${spinstr#?}
        if [ -t 1 ]; then printf "\r%-${printLength}s[%c]" "$text" "$spinstr"; fi
        local spinstr=$temp${spinstr%"$temp"}
        sleep ${delay}
    done
    wait ${pid}
    local rc=$?

    [ -t 1 ] && printf "\r"
    if [[ "$rc" -eq "0" ]]; then
        printf "%-${printLength}s[${fggrn}OK${normal}]\n" "$text"
        return ${rc}
    else
        printf "%-${printLength}s[${fgred}NOK${normal}]\n" "$text"
        return ${rc}
    fi
}

echo

# load up the minimal dump
(cat ${mysqlDir}/minimal-dump.sql | mysql --defaults-extra-file=${tmpMyCnf}) >> ${debugLog} 2>&1 &
spinner "- Importing minimal dump structure"
(cat ${mysqlDir}/minimal-data.sql | mysql --defaults-extra-file=${tmpMyCnf}) >> ${debugLog} 2>&1 &
spinner "- Importing minimal dump data"

echo
echo "Done."
echo "Debug-log: ${debugLog}"
