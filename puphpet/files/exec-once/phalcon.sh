# Install phalcon
sudo apt-add-repository ppa:phalcon/stable
sudo apt-get update
sudo apt-get install php5-phalcon

# Install packages
sudo apt-get install php5-dev php5-mysql gcc libpcre3-dev

# Compile phalcon
git clone --depth=1 git://github.com/phalcon/cphalcon.git
cd cphalcon/build
sudo ./install

# Add extension
touch /etc/php5/fpm/conf.d/30-phalcon.ini
echo 'extension=phalcon.so' > /etc/php5/fpm/conf.d/30-phalcon.ini

# Restart webservice
sudo service apache2 restart
sudo service php5-fpm restart