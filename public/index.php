<?php
error_reporting(E_ALL);
use Phalcon\Mvc\Application;

try {
    define('APP_PATH', realpath('..') . '/');
    defined('APP_ENV') || define('APP_ENV', (getenv('APP_ENV') ? getenv('APP_ENV') : 'production'));

    /** Read the configuration */
    require APP_PATH . 'app/bootstrap/config.php';

    /** Auto-loader configuration */
    require APP_PATH . 'app/bootstrap/loader.php';

    /** Load application services */
    require APP_PATH . 'app/bootstrap/services.php';

    $application = new Application($di);
    echo $application->handle()->getContent();

} catch (Exception $e) {
    if (APP_ENV == 'dev') {
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}